import com.test.view.ModelBean;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.view.faces.bi.component.pivotTable.UIPivotTable;
import oracle.adf.view.faces.bi.model.DataModel;

public class testPivotBean {
    
    private List<ModelBean> datos;
    private UIPivotTable pivotComponent;

    public testPivotBean(){
        
        datos = new ArrayList<ModelBean>();
        ModelBean modelo = new ModelBean();
        modelo.setYear("2015");
        modelo.setSalary(350);
        modelo.setDeparment("Recursos Humanos");
        datos.add(modelo);
        modelo = new ModelBean();
        modelo.setYear("2016");
        modelo.setSalary(400);
        modelo.setDeparment("Recursos Humanos");
        datos.add(modelo);
        modelo = new ModelBean();
        modelo.setYear("2017");
        modelo.setSalary(450);
        modelo.setDeparment("Recursos Humanos");
        datos.add(modelo);
        modelo = new ModelBean();
        modelo.setYear("2017");
        modelo.setSalary(500);
        modelo.setDeparment("Economia");
        datos.add(modelo);
        
        DataModel aux = pivotComponent.getDataModel();
    }

    public void setDatos(List<ModelBean> datos) {
        this.datos = datos;
    }

    public List<ModelBean> getDatos() {
        return datos;
    }

    public void setPivotComponent(UIPivotTable pivotComponent) {
        this.pivotComponent = pivotComponent;
    }

    public UIPivotTable getPivotComponent() {
        return pivotComponent;
    }
}
