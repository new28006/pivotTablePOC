package com.test.view;

import java.io.Serializable;

public class ModelBean implements Serializable {
    @SuppressWarnings("compatibility:-6777364731459525143")
    private static final long serialVersionUID = 1L;
    
    private String year;
    private Integer salary;
    private String deparment;

    public ModelBean() {
        super();
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setDeparment(String deparment) {
        this.deparment = deparment;
    }

    public String getDeparment() {
        return deparment;
    }
}
